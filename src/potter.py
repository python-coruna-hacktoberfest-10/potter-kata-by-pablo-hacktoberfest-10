def is_bad_formated(basket):
    return basket is None or not isinstance(basket, list) or len(basket) != 5

def calculate(basket):
    price = 0
    unit_price = 8
    discount = [0, .05, .1, 0.15, 0.25]

    if is_bad_formated(basket):
        raise ValueError('Bad format')

    while sum(basket) != 0:
        elements_in_set = 0
        for i, _ in enumerate(basket):

            if (basket[i] != 0):
                elements_in_set = elements_in_set + 1
                basket[i] = basket[i] - 1

        set_full_price = unit_price * elements_in_set
        set_price_discount = set_full_price * discount[elements_in_set - 1]

        price = price + set_full_price - set_price_discount

    return price
