import unittest, types
from src.potter import calculate


class TestPotter(unittest.TestCase):

    def test_calculate_is_function(self):
        self.assertEqual(type(calculate), types.FunctionType )

    @unittest.expectedFailure
    def test_basket_format_badlen(self):
        self.assertRaises(ValueError, calculate([0]))

    @unittest.expectedFailure
    def test_basket_format_not_list(self):
        self.assertRaises(ValueError, calculate({}))

    @unittest.expectedFailure
    def test_basket_format_None(self):
        self.assertRaises(ValueError, calculate(None))

    def test_zero_book_price(self):
        self.assertEqual(calculate([0, 0, 0, 0, 0]), 0)


    def test_one_book_price(self):
        self.assertEqual(calculate([1, 0, 0, 0, 0]), 8)
        self.assertEqual(calculate([0, 1, 0, 0, 0]), 8)
        self.assertEqual(calculate([0, 0, 1, 0, 0]), 8)
        self.assertEqual(calculate([0, 0, 0, 1, 0]), 8)
        self.assertEqual(calculate([0, 0, 0, 0, 1]), 8)


    def test_two_unique_book_price(self):
        self.assertEqual(calculate([1, 1, 0, 0, 0]), 15.2)
        self.assertEqual(calculate([1, 0, 1, 0, 0]), 15.2)
        self.assertEqual(calculate([1, 0, 0, 1, 0]), 15.2)
        self.assertEqual(calculate([1, 0, 0, 0, 1]), 15.2)
        self.assertEqual(calculate([0, 1, 1, 0, 0]), 15.2)
        self.assertEqual(calculate([0, 1, 0, 1, 0]), 15.2)
        self.assertEqual(calculate([0, 1, 0, 0, 1]), 15.2)
        self.assertEqual(calculate([0, 0, 1, 1, 0]), 15.2)
        self.assertEqual(calculate([0, 0, 1, 0, 1]), 15.2)
        self.assertEqual(calculate([0, 0, 0, 1, 1]), 15.2)

    def test_three_unique_book_price(self):
        self.assertEqual(calculate([1, 1, 1, 0, 0]), 21.6)
        self.assertEqual(calculate([1, 0, 1, 1, 0]), 21.6)
        self.assertEqual(calculate([1, 0, 0, 1, 1]), 21.6)
        self.assertEqual(calculate([1, 1, 0, 1, 0]), 21.6)
        self.assertEqual(calculate([1, 1, 0, 0, 1]), 21.6)
        self.assertEqual(calculate([1, 0, 1, 0, 1]), 21.6)
        self.assertEqual(calculate([0, 1, 1, 1, 0]), 21.6)
        self.assertEqual(calculate([0, 0, 1, 1, 1]), 21.6)

    def test_four_unique_book_price(self):
        self.assertEqual(calculate([1, 1, 1, 1, 0]), 27.2)

    def test_five_unique_book_price(self):
        self.assertEqual(calculate([1, 1, 1, 1, 1]), 30)

    def test_one_repeated_book(self):
        self.assertEqual(calculate([2, 0, 0, 0, 0]), 16)
        self.assertEqual(calculate([0, 0, 0, 0, 100]), 800)

    def test_multiple_repeated_books(self):
        self.assertEqual(calculate([2, 1, 1, 0, 0]), 29.6)
        self.assertEqual(calculate([5, 5, 5, 5, 5]), 150)

if __name__ == '__main__':
    unittest.main()
